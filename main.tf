resource "aws_key_pair" "minecraft" {
  public_key = var.minecraft_server_public_key
  key_name   = "Mincraft Server"
}

resource "aws_security_group" "minecraft" {
  vpc_id      = aws_vpc.minecraft.id
  name        = "Minecraft"
  description = "Minecraft Server"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "minecraft" {
  for_each          = local.minecraft_protocols
  security_group_id = aws_security_group.minecraft.id
  description       = "Minecraft ports w/ ${each.value} protocol."
  type              = "ingress"
  from_port         = 19132
  to_port           = 19133
  protocol          = each.value
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "minecraft_ssh" {
  security_group_id = aws_security_group.minecraft.id
  description       = "SSH access for maintenance."
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "minecraft_internet" {
  security_group_id = aws_security_group.minecraft.id
  description       = "Internet access."
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

data "aws_ami" "minecraft" {
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22*-amd64*"]
  }

  filter {
    name   = "platform-details"
    values = ["Linux/UNIX"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "is-public"
    values = ["true"]
  }

  most_recent = true
  owners      = ["amazon", "aws-marketplace"]
}

resource "aws_ebs_volume" "minecraft" {
  availability_zone = aws_subnet.minecraft.availability_zone
  type              = "gp3"
  size              = 50
  tags              = {
    Name = "Minecraft Worlds"
  }
}

resource "aws_volume_attachment" "minecraft" {
  device_name                    = "/dev/sdf"
  volume_id                      = aws_ebs_volume.minecraft.id
  instance_id                    = aws_instance.minecraft.id
  stop_instance_before_detaching = true
}

resource "aws_instance" "minecraft" {
  ami                         = "ami-02faa56de5d75ba00"
  subnet_id                   = aws_subnet.minecraft.id
  instance_type               = "t2.xlarge"
  disable_api_termination     = true
  key_name                    = aws_key_pair.minecraft.key_name
  vpc_security_group_ids      = [aws_security_group.minecraft.id]
  user_data_replace_on_change = true


  user_data = templatefile("files/scripts/sh/minecraft-setup.sh.tftpl", {
    minecraft_server_download  = var.minecraft_server_download,
    minecraft_service_file     = file("${path.module}/files/config/systemd/minecraft.service")
    minecraft_start_script     = file("${path.module}/files/scripts/sh/start.sh")
    minecraft_stop_script      = file("${path.module}/files/scripts/sh/stop.sh")
    minecraft_allowlist_json   = file("${path.module}/files/config/minecraft/allowlist.json")
    minecraft_permissions_json = file("${path.module}/files/config/minecraft/permissions.json")
    minecraft_properties = templatefile("${path.module}/files/config/minecraft/server.properties", {
      minecraft_chat     = var.minecraft_chat,
      minecraft_world    = var.minecraft_world,
      minecraft_gamemode = var.minecraft_gamemode
    })
  })

  metadata_options {
    instance_metadata_tags = "enabled"
    http_endpoint          = "enabled"
  }

  tags = {
    "Name" = "Minecraft"
  }
}

data "aws_route53_zone" "bradhandy-dev" {
  name = "bradhandy.dev."
}

resource "aws_eip" "minecraft" {
  instance = aws_instance.minecraft.id
  vpc      = true
}

resource "aws_route53_record" "minecraft" {
  zone_id = data.aws_route53_zone.bradhandy-dev.zone_id
  name    = "minecraft.${data.aws_route53_zone.bradhandy-dev.name}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.minecraft.public_ip]
}