resource "aws_vpc" "minecraft" {
  cidr_block = "148.10.0.0/16"

  tags = {
    "Name" = "Minecraft"
  }
}

resource "aws_subnet" "minecraft" {
  vpc_id            = aws_vpc.minecraft.id
  availability_zone = "us-east-1a"
  cidr_block        = "148.10.255.0/24"

  tags = {
    "Name" = "Minecraft"
  }
}

resource "aws_internet_gateway" "minecraft" {
  tags = {
    "Name" = "Minecraft Gateway"
  }
}

resource "aws_internet_gateway_attachment" "minecraft" {
  vpc_id              = aws_vpc.minecraft.id
  internet_gateway_id = aws_internet_gateway.minecraft.id
}

resource "aws_route_table" "minecraft" {
  vpc_id = aws_vpc.minecraft.id

  tags = {
    "Name" = "minecraft_rt"
  }
}

resource "aws_route" "minecraft_internet" {
  route_table_id         = aws_route_table.minecraft.id
  gateway_id             = aws_internet_gateway.minecraft.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "minecraft" {
  route_table_id = aws_route_table.minecraft.id
  subnet_id = aws_subnet.minecraft.id
}

