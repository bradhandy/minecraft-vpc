terraform {
  required_version = "~> 1.4"

  cloud {
    organization = "bradhandy-dev"
    workspaces {
      name = "minecraft-vpc"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.61"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Service = "minecraft"
    }
  }
}