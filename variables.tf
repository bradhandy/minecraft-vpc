locals {
  minecraft_protocols = toset(["tcp", "udp"])
}

variable "minecraft_server_public_key" {
  description = "The public key value for the minecraft server."
  type        = string
}

variable "minecraft_server_download" {
  description = "The URL for the Minecraft Server bundle."
  type        = string
}

variable "minecraft_chat" {
  description = "Chat setting for the Minecraft server."
  type        = string
  default     = "None"
  validation {
    condition     = contains(["None", "Disabled", "Dropped"], var.minecraft_chat)
    error_message = "Chat setting can be one of: 'None', 'Dropped', or 'Disabled'."
  }
}

variable "minecraft_world" {
  description = "Name of the world to use."
  type        = string
  default     = "Dad and Kids"
}

variable "minecraft_gamemode" {
  description = "Survival or Creative game play."
  type        = string
  default     = "survival"
  validation {
    condition     = contains(["survival", "creative", "adventure"], var.minecraft_gamemode)
    error_message = "Game mode must be 'survival', 'creative', or 'adventure'."
  }
}